#!/bin/bash

for file in test_progs/*.s; do
    file=$(echo $file | cut -d'.' -f1)
    echo "Assembling $file"
    # How do you assemble a testcase?
    make assembly SOURCE=$file.s
    echo "Running $file"
    # How do you run a testcase?
    make all
    echo "Saving $file output"
    # How do you want to save the output?
    # What files do you want to save?
    grep '@@@' program.out > $file.out  
    # program.out for $file
    grep '' writeback.out > $file.wbout
    # writeback.out for $file
done



for file in test_progs/*.c; do
    file=$(echo $file | cut -d'.' -f1)
    echo "Compiling $file"
    # How do you assemble a testcase?
    make program SOURCE=$file.c
    echo "Running $file"
    # How do you run a testcase?
    make all
    echo "Saving $file output"
    # How do you want to save the output?
    # What files do you want to save?
    grep '@@@' program.out > $file.out  
    # program.out for $file
    grep '' writeback.out > $file.wbout
    # writeback.out for $file
done

CORRECT_DIR=../eecs470_f20_project3_guoqin_copy
# correct result's directory

# diff the test cases with correct ones
for file in test_progs/*out; do
    diff $file $CORRECT_DIR/$file
    error=$?
    if [ $error -eq 0 ]
        then
        echo "$file PASSED"
    elif [ $error -eq 1 ]
        then
        echo "$file FAILED"
        # break
        # break the loop when failed
    else
        echo "There was something wrong with the diff command"
    fi
done

